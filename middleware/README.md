# MIDDLEWARE

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your application middleware.
Middleware let you define custom functions that can be run before rendering either a page or a group of pages.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing#middleware).

Penjelasan:

- Middleware digunakan untuk mengecek apakah user sudah login atau belum. Ketika sudah login akan diarahkan kedalam halaman dashboard.
- Middlelware juga digunakan untuk meredirect apabila user yang belum login mengakses halaman yang membutuhkan login terlebih dahulu
