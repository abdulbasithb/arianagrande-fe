import axios from 'axios'
axios.defaults.baseURL = process.env.BASE_URL

export const state = () => ({
  // user
  user: null,
  token: null,
  isUserLoading: false,
  errorLogin: null,
})

export const getters = {
  user: (state) => {
    return state.user
  },
  token: (state) => {
    return state.token
  },
  isUserLoading: (state) => {
    return state.isUserLoading
  },
  errorLogin: (state) => state.errorLogin,
}

export const mutations = {
  SET_USER(state, data) {
    state.user = data
    if (process.client) {
      localStorage.setItem('user', JSON.stringify(data))
    }
  },
  SET_TOKEN(state, data) {
    state.token = data
    if (process.client) {
      localStorage.setItem('token', JSON.stringify(data))
    }
  },
  SET_USER_LOADING(state, data) {
    state.isUserLoading = data
  },
  SET_ERROR_LOGIN(state, data) {
    state.errorLogin = data
  },
}

export const actions = {
  async doLogin({ dispatch, commit }, payload) {
    try {
      commit('SET_USER_LOADING', true)
      const response = await this.$axios.$post(`auth/local`, {
        identifier: payload.email,
        password: payload.password,
      })

      commit('SET_USER', response.user)
      commit('SET_TOKEN', response.jwt)
      commit('SET_USER_LOADING', false)
      dispatch('setAxios')
    } catch (error) {
      commit('SET_USER_LOADING', false)
      console.log(error)
    }
  },
  clearUserAndToken({ commit }) {
    commit('SET_TOKEN', null)
    commit('SET_USER', null)
    if (process.client) {
      localStorage.removeItem('user')
      localStorage.removeItem('token')
    }
    this.$axios.setToken(false)
    this.$axios.setHeader('Content-Type', false, ['post', 'put'])
  },
  setAxios({ state }) {
    // Adds header: `Content-Type: application/x-www-form-urlencoded` to only post requests
    this.$axios.setHeader('Content-Type', 'application/x-www-form-urlencoded', [
      'post',
      'put',
    ])
    this.$axios.setToken(state.token, 'Bearer')
  },
}
